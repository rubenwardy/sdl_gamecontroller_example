# SDL_GameController Example

See [my blog post for an explanation/write up](https://blog.rubenwardy.com/2023/01/24/using_sdl_gamecontroller/).

Usage:

```
cmake .
make -j7
./sdl_controller
```
